//E4.23, program calculates the
public class SodaCan {
double height;
double diameter;
double volume;
double surfaceArea;
public static double PI = 3.14;
public  SodaCan(double height, double diameter){
    this.height=height;
    this.diameter=diameter;
}
public double getVolume () {
    volume = (diameter/2)* (diameter/2)*PI*height;
    return volume;
}

    public double getSurfaceArea () {
        surfaceArea = ((height*diameter)/2) * 2*PI +( (2*PI) * (diameter/2) * (diameter/2));
        return surfaceArea;
    }

}

class SodaCanTest {
   public static void main(String[] argus){
       SodaCan sodaCan1 = new SodaCan(2,6);
       System.out.println("Calculating specified dimensions for a soda can, h=2, d=3 " );
       System.out.println("Surface Area = " + sodaCan1.getSurfaceArea() + " units squared" );
       System.out.println("Volume = " + sodaCan1.getVolume() + " units squared");

   }

}