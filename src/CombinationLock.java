public class CombinationLock {
    int secret1, secret2, secret3;
    int secret1Reset, secret2Reset, secret3Reset;
    int numberOfDigits = 1;

    boolean openLocker = true;

//constructor
            public CombinationLock(int secret1, int secret2, int secret3) {
                this.secret1 = secret1;
                this.secret2 = secret2;
                this.secret3 = secret3;
            }

//reset()
                public void reset() {
                    secret1Reset =0 ;
                    secret2Reset =0 ;
                    secret3Reset =0 ;
                    openLocker = true;
                }

//turnLeft()
            public void turnLeft(int ticks) {
                if(numberOfDigits != 2) {
                    openLocker = false;
                }
                else {
                    secret2Reset = ticks;
                    numberOfDigits++;
                }
            }

//turnRight()
    public void turnRight(int ticks) {
        if (numberOfDigits == 1) {
            secret1Reset = ticks;
            numberOfDigits++;
        } else if (numberOfDigits == 3) {
            secret3Reset = ticks;
            numberOfDigits++;
        } else {
            openLocker = false;
        }
    }

//openLocker()
    public boolean openLocker() {
        if (openLocker) {
            //int secret1Rest=0;
            if (secret1Reset == secret1 && secret2Reset == secret2 && secret3Reset == secret3) {
                return true;
            }
              else {
                    return false;
                }
         }
        else {
            return false;
            }
        }


}

//CombinationLockTest {}
class CombinationLockTest{
    public static void main(String args[]){
        CombinationLock lock1 = new CombinationLock(2,19,27);
        System.out.println("Step 1: Reset the locker");
        lock1.reset();
        System.out.println("Step 2: Turn the locker towards the left direction, 2 ticks");
        lock1.turnLeft(2);
        System.out.println("Step 3: Turn the locker towards the right direction, 19 ticks");
        lock1.turnRight(19);
        System.out.println("Step 4: Turn the locker towards the left direction, 29 ticks");
        lock1.turnLeft(27);
        lock1.openLocker();
        System.out.println("Checking combination ... Locker is open (true/false) " + lock1.openLocker());

        System.out.println("Step 1: Reset the locker");
        lock1.reset();
        System.out.println("Step 2: Turn the locker towards the left direction, 2 ticks");
        lock1.turnLeft(2);
        System.out.println("Step 3: Turn the locker towards the right direction, 19 ticks");
        lock1.turnRight(19);
        System.out.println("Step 4: Turn the locker towards the left direction, 29 ticks");
        lock1.turnLeft(27);
        lock1.openLocker();
        System.out.println("Checking combination ... Locker is open (true/false) " + lock1.openLocker());

        }
}

