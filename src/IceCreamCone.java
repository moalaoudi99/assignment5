//Exercise 4.22, program calculates the  surface area and volume of a cone, ice cream cone.
public class IceCreamCone {

   // import java.math.*;
    double height ;
    double radius ;
    double diagonalHeight ;
    double surfaceArea;
    double volume;


    public static double PI = 3.14;
    public IceCreamCone (double height, double radius) {
        this.height = height;
        this.radius = radius;
        this.diagonalHeight=(height*height)+(radius*radius);
        this.diagonalHeight = Math.sqrt(diagonalHeight);
    }

    public double getSurfaceArea(){
    surfaceArea = PI*radius*diagonalHeight + PI * radius*radius ;
    return surfaceArea;

    }
    public double getVolume() {
        volume = (PI*height*radius*radius)/3;
        return volume;
        }


        }


class IceCreamTest {
    public static void main(String[] args){
        IceCreamCone cone1 = new IceCreamCone(2,3);
        System.out.println("Calculating specified dimensions for ice cream cone, h=2, rad=3 " );
        System.out.println("Surface Area = " + cone1.getSurfaceArea() + " units squared" );
        System.out.println("Volume = " + cone1.getVolume() + " units squared");

    }

}